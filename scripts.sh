#aula https://www.youtube.com/watch?v=eXKg9B5ooaY

docker login

docker build -t app \
    projeto

docker tag app amsousa95/node-postgres
docker push amsousa95/node-postgres

MINIKUBE
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

KUBECTL
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version --client


minikube start --driver=docker
minikube config set driver docker # definir como padrao para proximas vezes
sudo usermod -aG docker $USER && newgrp docker

minikube start
minikube dashboard # abre no navegador a interface, no pods e possivel acessar o terminal de cada service

kubectl get nodes 
kubectl describe nodes 
# minikube kubectl -- get nodes -A
# minikube kubectl -- describe nodes -A

cd kubeconfig

kubectl create -f postgres-sts.json # nome do arquivo q quer criar

kubectl get statefulset 
kubectl get pod
kubectl logs postgres-0

kubectl describe sts postgres
kubectl describe pod postgres-0

kubectl get svc # exibe services

kubectl describe service postgres-svc # bom usar pra ver se tem endpoint, senao vai ficar dando not found

kubectl get deploy # mostrar componentes
kubectl describe deploy api-heroes

kubectl get pod 
kubectl describe pod api-heroes-6f9b65c87d-glqtc # usar get pode pra pegar o name pq e loadbalancer pro publico nesse caso diferente do BD q e clusterip ficando interno o acesso

kubectl logs api-heroes-5d96dd4c87-wxvhf
kubectl logs -f api-heroes-5d96dd4c87-wxvhf

kubectl apply -f api-deployment.json # atualiza componentes atualizando existentes ou criando
kubectl get pod # checar se criou novas replicas por exemplo

minikube service api-heroes-svc --url # ver url de acesso pois minikube nao e de producao onde o proprio kubernetes exibia o url loadbalancer, aqui ele fica sempre pendente
# se der erro de permissao
# sudo usermod -aG docker ${USER}
# sudo chmod 666 /var/run/docker.sock

# tente acessar ip acessado http://ipmostrado/documentation

# no arquivo run.sh, trocar para url gerada e tente rodar no terminal "sh run.sh" para criar (exemplo da 1 erro mas adiciona o primeiro registro ignore)

kubectl get pod -w # watch se tiver fica em tempo real no terminal 
kubectl logs -f api-heroes-5d96dd4c87-wxvhf # -f exibe log tempo real
kubectl delete -f . # deleta do kubernets os arquivos da pasta 
kubectl create -f . # cria todos arquivos na pasta

minikube delete